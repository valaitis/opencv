#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch (depth) {
    case CV_8U: r = "8U"; break;
    case CV_8S: r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default: r = "User"; break;
  }

  r += "C";
  r += (chans + '0');

  return r;
}

int main(int argc, char** argv) {
  cv::VideoCapture cap;
  cap.open(1);
  cap.set(CV_CAP_PROP_FOURCC, CV_FOURCC('Y','U','Y','V'));
  cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
  cap.set(CV_CAP_PROP_FPS, 30);

  double width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  double height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
  double fps = cap.get(CV_CAP_PROP_FPS);
  //double f = cap.get(CV_CAP_PROP_FOURCC);
  //char* fourcc = (char*) (&f); // reinterpret_cast

  std::cout << "OpenCV " << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION << std::endl;
  std::cout << width << " x " << height << " @ " << fps << std::endl;

  cv::namedWindow("rgb", CV_WINDOW_AUTOSIZE);

  bool first = 1;
  int cnt = 0;

  while(1) {
    cnt++;
    cv::Mat frame;
    cap.read(frame);

    if (first) {
      std::cout << "stride: " << frame.step  << " dims: " << frame.dims << " type: " << type2str(frame.type()) << std::endl;
      first = 0;
    }

    //cv::Mat bgr[3];
    //cv::split(frame, bgr);

    cv::imshow("rgb", frame);
    //cv::imshow("blue", bgr[0]);
    //cv::imshow("green", bgr[1]);
    //cv::imshow("red", bgr[2]);

    //-----hist--
    cv::Mat frame_gray;
    cv::Mat hist;
    int histSize = 256;
    float range[] = {0, 256};
    const float* histRange = {range};

    cv::cvtColor(frame, frame_gray, CV_BGR2GRAY);
    cv::calcHist(&frame_gray, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, true, false);

    int hist_w = 256;
    int hist_h = 100;
    int bin_w = cvRound((double)hist_w / histSize);
    cv::Mat hist_image(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::normalize(hist, hist, 0, hist_image.rows, cv::NORM_MINMAX, -1, cv::Mat());

    for(int i = 1; i < histSize; i++) {
      cv::line(hist_image, cv::Point(bin_w * (i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
                           cv::Point(bin_w * (i), hist_h - cvRound(hist.at<float>(i))),
                           cv::Scalar(255, 255, 255), 1, 8, 0);
    }

    cv::imshow("histogram", hist_image);
    //-----------

    //-----laplacian--
    cv::GaussianBlur(frame_gray, frame_gray, cv::Size(3, 3), 0, 0, cv::BORDER_DEFAULT);
    cv::Mat frame_edges, frame_edges_abs;
    cv::Laplacian(frame_gray, frame_edges, CV_16S, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::convertScaleAbs(frame_edges, frame_edges_abs);
    cv::imshow("edges", frame_edges_abs);

    cv::Scalar mean;
    cv::Scalar stddev;
    cv::meanStdDev (frame_edges, mean, stddev);
    double mean_pxl = mean.val[0];
    double stddev_pxl = stddev.val[0];
    cv::Mat frame_numbers(100, 256, CV_8U);
    frame_numbers = cv::Scalar(0, 0, 0);
    std::ostringstream strs_1;
    strs_1 << mean_pxl;
    std::string str_1 = strs_1.str();
    cv::putText(frame_numbers, "mean: " + str_1, cvPoint(30,30), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255, 255, 255), 1, CV_AA);
    std::ostringstream strs_2;
    strs_2 << stddev_pxl;
    std::string str_2 = strs_2.str();
    cv::putText(frame_numbers, "stddev: " + str_2, cvPoint(30,60), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255, 255, 255), 1, CV_AA);
    cv::imshow("numbers", frame_numbers);
    //----------------

    char key = (char)cv::waitKey(1);
    if (key == (char)27) {
      std::cout << "esc key pressed by user" << std::endl;
      break;
    } else if (key == 'p' || key == 'P') {
      std::vector<int> compression_params;
      compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
      compression_params.push_back(0); // compression 0-9
      std::ostringstream strs;
      strs << cnt;
      std::string str = strs.str();
      cv::imwrite("screenshot_" + str + ".png", frame, compression_params);
    }
  }
  return 0;
}

